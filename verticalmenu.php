<?php
$vMenu = array (
    'Home'      => 'index.php',
    'Contact'   => 'contact.php',
    'About'     => 'about.php',
);

$hMenu = array (
    'Search'        => 'http://google.com',
    'Information'   => 'https://www.google.ru/services/?fg=1',
    'Maps'          => 'https://www.google.com.ua/maps/search/google+maps/@37.4219995,-122.0928123,15z/data=!3m1!4b1',
);

function getMenu ($menu, $vertical = true) {
    $style = '';
    if (!$vertical) {
        $style = ' style = "display: inline; margin-right:15px" ';
    }

    echo '<ul>';

    foreach ($menu as $link => $href) {
        echo "<li$style><a href =\"$href\">$link</a></li>";
    }

    echo '</ul>';
}
?>
<html>
<head>
    <meta charset = "utf8">
    <title> Меню </title>
</head>
<body>
    <h1> Меню</h1>
    <?php
        getMenu ($vMenu);

        getMenu ($hMenu, false);
    ?>
</body>
</html>



